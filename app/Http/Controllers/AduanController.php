<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Aduan;
use Validator;

class AduanController extends Controller
{
    // Menampilkan form aduan
    public function createForm()
    {
        return view('aduan.create');
    }

    // Menyimpan aduan yang dikirimkan oleh user
    public function store(Request $request)
    {

        $judul = $request->input('judul');
        $deskripsi = $request->input('deskripsi');

        // Simpan aduan ke dalam database
        if (is_string($judul) && is_string($deskripsi)) {
            $aduan = new Aduan();
            $aduan->judul = $request->input('judul');
            $aduan->deskripsi = $request->input('deskripsi');
            $aduan->save();

        if ($aduan->save()) {
            // Aduan berhasil terkirim
            return redirect('/buat-aduan')->with('success', 'Aduan berhasil terkirim.');
        } else {
            // Aduan gagal terkirim
            return redirect('/buat-aduan')->with('error', 'Pesan gagal terkirim. Coba Lagi.');
        }

        } else {
            return redirect('/buat-aduan')->with('error', 'Pesan gagal terkirim. Coba Lagi.');
        }

    
    }
}
