<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Aduan;
use App\Models\Tanggapan;
use Validator;

class TanggapanController extends Controller
{
    // Menampilkan seluruh aduan yang masuk
    public function index()
    {
        $aduanList = Aduan::all();
        return view('tanggapan.index', ['aduanList' => $aduanList]);
    }

    // Menampilkan halaman aduan yang dipilih oleh customer service
    public function show($id)
    {
        $aduan = Aduan::findOrFail($id);
        return view('tanggapan.show', ['aduan' => $aduan]);
    }

    // Menyimpan tanggapan aduan dari Customer Service
    public function store(Request $request, $id)
    {

        // Simpan tanggapan ke dalam database
        $tanggapan = $request->input('tanggapan');
        if($tanggapan){
            $tanggapan = new Tanggapan();
            $tanggapan->aduan_id = $id;
            $tanggapan->tanggapan = $request->input('tanggapan');
            $tanggapan->save();

            if ($tanggapan->save()) {
                // Aduan berhasil terkirim
                return redirect("/lihat-aduan/$id")->with('success', 'Tanggapan berhasil terkirim.');
            } else {
                // Aduan gagal terkirim
                return redirect("/lihat-aduan/$id")->with('error', 'Pesan gagal terkirim. Coba Lagi.');
            }
        } else {
            return redirect("/lihat-aduan/$id")->with('error2', 'Pesan gagal terkirim. Coba Lagi.');
        }

    }
}
