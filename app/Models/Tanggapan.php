<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tanggapan extends Model
{
    protected $table = 'tanggapans';

    protected $fillable = ['tanggapan', 'aduan_id'];

    public function aduan()
    {
        return $this->belongsTo(Aduan::class);
    }
}

