<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aduan extends Model
{
    protected $table = 'aduans';

    protected $fillable = ['judul', 'deskripsi'];

    public function tanggapan()
    {
        return $this->hasMany(Tanggapan::class);
    }
}
