@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Buat Aduan</h2>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="POST" action="/kirim-aduan">
            @csrf
            <div class="form-group">
                <label for="judul">Judul Aduan</label>
                <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul') }}">
            </div>
            <div class="form-group">
                <label for="deskripsi">Deskripsi Aduan</label>
                <textarea class="form-control" id="deskripsi" name="deskripsi" rows="4">{{ old('deskripsi') }}</textarea>
            </div>
            <button type="submit" class="btn btn-primary">Kirim</button>
        </form>
    </div>

    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div> <br>
    @endif
@endsection
