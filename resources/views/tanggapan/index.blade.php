@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Daftar Aduan</h2>
        <ol class="first-ol">
            @foreach ($aduanList as $aduan)
                <li>
                    <a href="/lihat-aduan/{{ $aduan->id }}">{{ $aduan->judul }}</a>
                </li>
            @endforeach
        </ol>
    </div>
@endsection
