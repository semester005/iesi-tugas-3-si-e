@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Detail Aduan</h2>
        <h3>{{ $aduan->judul }}</h3>
        <p>{{ $aduan->deskripsi }}</p>
        <hr>
        <h4>Tanggapan:</h4>
        @foreach ($aduan->tanggapan as $tanggapan)
            <p>{{ $tanggapan->tanggapan }}</p>
        @endforeach
        <hr>
        <h4>Tanggapan Baru:</h4>
        @if ($errors->any())
            <div class="alert alert-danger2">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
       
        <form method="POST" action="/kirim-tanggapan/{{ $aduan->id }}">
            @csrf
            <div class="form-group">
                <label for="tanggapan">Tanggapan</label>
                <textarea class="form-control" id="tanggapan" name="tanggapan" rows="4">{{ old('tanggapan') }}</textarea>
            </div>
            <button type="submit" class="btn btn-primary">Kirim</button>
        </form>
    </div>

    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

@endsection
