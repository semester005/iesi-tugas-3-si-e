<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hotline Customer Service</title>
    <!-- <link rel="stylesheet" href="{{ asset('resources/css/app.css') }}"> -->
</head>
<body>
    <nav>
        <ul>
            <li><a href="/buat-aduan">Buat Aduan</a></li>
            <li><a href="/lihat-aduan">Lihat Aduan</a></li>
        </ul>
    </nav>
    
    @yield('content')

    <!-- <script src="{{ asset('js/app.js') }}"></script -->

    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
            <br><br>
            <a href="/buat-aduan" class="btn btn-primary">OK</a>
        </div>
    @endif

    @if(session('error2'))
        <div class="alert alert-danger2">
            {{ session('error2') }}
            <br><br>
            <a href="/lihat-aduan" class="btn btn-primary">OK</a>
        </div>
    @endif
</body>
</html>

<style>

    /* Gaya untuk navigasi */
    nav {
        background-color: #333;
        color: #fff;
        padding: 10px 0;
    }

    nav ul {
        list-style-type: none;
        padding: 0;
    }

    nav ul li {
        display: inline;
        margin-right: 20px;
    }

    nav ul li a {
        text-decoration: none;
        color: #fff;
        font-weight: bold;
        margin-top: 5px;
        margin-left: 25px; 
        margin-right: -25px; 
        margin-bottom: 0px; 
    }

    /* Gaya untuk form */
    .container {
        margin-top: 20px;
        padding: 20px;
        background-color: #f8f8f8;
        border: 1px solid #ddd;
        border-radius: 5px;
    }

    .form-group {
        margin-bottom: 15px;
    }

    label {
        font-weight: bold;
    }

    input[type="text"],
    textarea {
        width: 100%;
        padding: 10px;
        border: 1px solid #ddd;
        border-radius: 3px;
    }

    button.btn-primary {
        background-color: #007bff;
        color: #fff;
        border: none;
        padding: 10px 20px;
        cursor: pointer;
    }

    button.btn-primary:hover {
        background-color: #0056b3;
    }

    /* Gaya untuk daftar aduan */
    ul {
        list-style-type: none;
        padding: 0;
    }

    ul li {
        margin-bottom: 10px;
    }

    .first-ol {
        list-style-type: decimal;
        padding: 0;
    }

    .first-ol li{
        margin-bottom: 10px;
        /* background: hotpink; */
        color: darkblue;
        margin: 10px;
    }

    /* Gaya untuk halaman detail aduan */
    h2 {
        margin-top: 0;
    }

    /* Gaya untuk tanggapan */
    hr {
        border: 1px solid #ddd;
        margin: 20px 0;
    }

    /* Gaya untuk pesan sukses */
    .alert-success {
        background-color: #d4edda;
        color: #155724;
        border: 1px solid #c3e6cb;
        padding: 10px;
        margin-top: 20px;
        text-align: center;
    }

    /* Gaya untuk pesan kesalahan */
    .alert-danger {
        background-color: #f8d7da;
        color: #721c24;
        border: 1px solid #f5c6cb;
        padding: 10px;
        margin-top: 20px;
        text-align: center;
    }

    .alert-danger2 {
        background-color: #f8d7da;
        color: #721c24;
        border: 1px solid #f5c6cb;
        padding: 10px;
        margin-top: 20px;
        text-align: center;
    }

</style>
