<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AduanController;
use App\Http\Controllers\TanggapanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route untuk halaman form aduan (UC-001)
Route::get('/buat-aduan', [AduanController::class, 'createForm']);
Route::post('/kirim-aduan', [AduanController::class, 'store']);

// Route untuk halaman daftar aduan (UC-002)
Route::get('/lihat-aduan', [TanggapanController::class, 'index']);

// Route untuk halaman detail aduan dan form tanggapan (UC-002)
Route::get('/lihat-aduan/{id}', [TanggapanController::class, 'show']);
Route::post('/kirim-tanggapan/{id}', [TanggapanController::class, 'store']);

// Tambahan: Route untuk halaman utama
Route::get('/', function () {
    return view('welcome');
});
